<?php
include_once '../../controllers/User.controller.php';

  $user_obj = new UserController();
  $option = isset($_POST['option']) ? $_POST['option'] : "";

  switch ($option) {

    case 'login':
      $email = isset($_POST['email']) ? $_POST['email'] : "";
      $password = isset($_POST['password']) ? $_POST['password'] : "";

      $user = $user_obj->loginUser($email);

      $hash_BD = $user->password_user;

      if (!$user) {
        echo json_encode(array(
          'status' => 'invalid',
          'message' => '<div class="alert alert-info  text-center" role="alert">
          <strong>Error, Usuario Incorrecto.</strong></div>'
        ));
      } else if (password_verify($password, $hash_BD)) {
        session_start();
        $_SESSION["user"] = $user->name_user;

        echo json_encode(array(
          'status' => 'valid',
          'message' => '<div class="alert-success text-center">
            <strong>Bienvenid@</strong></div>'
        ));
      } else {
        echo json_encode(array(
          'status' => 'invalid',
          'message' => '<div class="alert alert-info  text-center" role="alert">
        <strong>Error, Usuario o Contraseña Incorrecta.</strong></div>'
        ));
      }
      break;

    case 'view':
      $id = isset($_POST['viewData']) ? $_POST['viewData'] : "";
      $user = $user_obj->showUser($id);
      if ($user) {
        echo '<table class="table table-dark">
                <tbody>
                    <tr>
                        <th>ID</th>
                        <td>' . $user->id_user_pk . '</td>
                    </tr>
                    <tr>
                        <th>Nombre</th>
                        <td>' . $user->name_user . '</td>
                    </tr>
                    <tr>
                        <th>Teléfono</th>
                        <td>' . $user->phone_user . '</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>' . $user->email_user . '</td>
                    </tr>
                </tbody>
                </table>';
      } else {
        echo '<div class="text-center">
                            <img src="../../images/logo.png" alt="log" width="350" class="img-fluid" style="filter:invert(100%)" >
                            <div class="alert alert-success mt-2" role="alert">Usuario no encontrado <br>Consulte con el administrador del sistema</div>
                            </div>';
      }
      break;
    case 'create':
      $name = isset($_POST['name']) ? $_POST['name'] : "";
      $phone = isset($_POST['phone']) ? $_POST['phone'] : "";
      $email = isset($_POST['email']) ? $_POST['email'] : "";
      $password = isset($_POST['password']) ? $_POST['password'] : "";

      $user = $user_obj->insertUser($name, $phone, $email, $password);

      if ($user) {
        echo '<div class="alert-success text-center">
                <strong>Usuario con nombre :' . $name . ' creado exitosamente.</strong>
            </div>';
      } else {
        echo '<div class="alert alert-info  text-center" role="alert">
            <strong>Error, Consulte al administrador del sistema.</strong>
        </div>';
      }
      break;

    case 'view':
      $id = isset($_POST['viewData']) ? $_POST['viewData'] : "";
      $user = $user_obj->showUser($id);
      if ($user) {
        echo '<table class="table table-dark">
                <tbody>
                    <tr>
                        <th>ID</th>
                        <td>' . $user->id_user_pk . '</td>
                    </tr>
                    <tr>
                        <th>Nombre</th>
                        <td>' . $user->name_user . '</td>
                    </tr>
                    <tr>
                        <th>Teléfono</th>
                        <td>' . $user->phone_user . '</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>' . $user->email_user . '</td>
                    </tr>
                </tbody>
                </table>';
      } else {
        echo '<div class="text-center">
                            <img src="../../images/logo.png" alt="log" width="350" class="img-fluid" style="filter:invert(100%)" >
                            <div class="alert alert-success mt-2" role="alert">Usuario no encontrado <br>Consulte con el administrador del sistema</div>
                            </div>';
      }
      break;

    case "delete":
      $id = isset($_POST['id']) ? ($_POST['id']) : "";
      $user_obj->deleteUser($id);
      if ($user_obj) {
        echo '<div class="alert-success text-center">
                <strong>Usuario con ID :' . $id . ' eliminado exitosamente.</strong>
            </div>';
      } else {
        echo '<div class="alert alert-info  text-center" role="alert">
                <strong>Error, Consulte con el administrador del sistema</strong>
            </div><div id="sendEdit"></div>';
      }

      break;

    case 'close':
      session_start();
      session_destroy();
      break;

    default:
      break;
  }
  ?>
