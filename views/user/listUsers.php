<?php
session_start();

if (!isset($_SESSION["user"])) {
  header("Location:../login/login.php");
}
include_once '../../controllers/User.controller.php';
include_once '../header.php';
include_once '../footer.php';
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="../../css/main.css">
  <title>Usuarios</title>
</head>

<body class="d-flex flex-column" style="min-height: 100vh">

  <?php
  $user_obj = new UserController();
  $users = $user_obj->listUsers();
  ?>

  <!-- toast insert -->
  <div class="toast" style="position: absolute; top: 20px; right: 10px;" data-autohide="true" data-delay="5000">
    <div class="toast-header">
      <img src="../../images/logo.png" class="rounded mr-2" alt="logo" width="100">
      <div class="row">
        <div class="col-12"><strong class="mr-auto">Mensaje</strong></div>
      </div>

      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body" id="toastMen">
    </div>
  </div>

  <!-- Modal create -->
  <div class="modal fade " id="createUser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Crear Usuario <i class="fa fa-user" aria-hidden="true"></i> </h5>
        </div>
        <div class="modal-body" style="background-color:rgb(63, 90, 110);color:wheat;font-size: 12pt;">
          <div class="row">
            <div class="col-md-12 col-12">
              <input type="hidden" class="form-control" name="option" id="option" value="create">
              <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Nombre" required="required">
              </div>
              <div class="form-group">
                <label for="phone">Telefono</label>
                <input type="phone" class="form-control" name="phone" id="phone" placeholder="Telefono" required="required">
              </div>
              <div class="form-group">
                <label for="email">Correo</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Correo electronico" required="required">
              </div>
              <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" required="required">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Cerrar</button>
          <button id="sendData" class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal view -->
  <div class="modal fade" id="viewUser" tabindex="-1" role="dialog" aria-labelledby="viewUser" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Datos del Usuario <i class="fas fa-user"></i></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body table-dark" id="showUser">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary viewClose" data-dismiss="modal"> Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal delete -->
  <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Eliminar Usuario
            <i class="fas fa-user"></i>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body del" style="background-color: #ffc3c9;font-size: 13pt;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-danger deleteSend">Eliminar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="container mb-5">
    <div class="row">
      <div class="col-md-12 col-md-offset-2">
        <div class="card-body d-flex">
          <h2>Usuarios</h2>
          <button type="button" id="closeSession" name="closeSession" class="btn btn-primary btn-sm ml-auto">Cerrar Sesión</button>
        </div>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col">
        <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#createUser">Insertar
          <i class="fas fa-user"></i>
        </button>
      </div>
    </div>
    <table class="table table-info" style="border-radius">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Nombre</th>
          <th scope="col" class="d-none d-sm-block">Email</th>
          <th scope="col">Opciones</th>
        </tr>
      </thead>
      <tbody id="user">
        <?php if ($users != null) {
          foreach ($users as $item) { ?>
            <tr>
              <td><?php echo $item->id_user_pk; ?></td>
              <td><?php echo $item->name_user; ?></td>
              <td class="d-none d-sm-block"><?php echo $item->email_user; ?></td>
              <td>
                <a class="btn btn-warning btn-lg viewUser col-12 col-lg-3 mt-2 mt-lg-0" role="button" data-toggle="modal" data-target="#viewUser" id="<?php echo $item->id_user_pk; ?>">Ver
                  <i class="fas fa-eye" style="font-size:12pt"></i>
                </a>
                <a class="btn  btn-danger btn-lg deleteUser col-12 col-lg-3 mt-2 mt-lg-0" role="button" data-toggle="modal" data-target="#deleteUser" id="<?php echo $item->id_user_pk; ?>">Eliminar
                  <i class="fas fa-trash" style="font-size:12pt"></i>
                </a>
              </td>
            </tr>
          </tbody>
        <?php }
    } else { } ?>
    </table>
    <div class="container mt-5 mb-5 py-5"></div>
    <script src="../../js/jquery-3.4.1.min.js"></script>
    <script src="../../js/main.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>