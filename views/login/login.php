<?php
include_once '../../controllers/User.controller.php';
include_once '../header.php';
include_once '../footer.php';
?>
<!doctype html>
<html lang="en">

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="../../css/main.css">
  <title>Login Usuarios</title>
</head>

<body>
  <!-- toast insert -->
  <div class="toast" style="position: absolute; top: 20px; right: 10px;" data-autohide="true" data-delay="5000">
    <div class="toast-header">
      <img src="../../images/logo.png" class="rounded mr-2" alt="logo" width="100">
      <div class="row">
        <div class="col-12"><strong class="mr-auto">Mensaje</strong></div>
      </div>

      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body" id="toastMen">
    </div>
  </div>
  <div class="card">
    <article class="card-body">
      <h4 class="card-title text-center mb-4 mt-1">Login</h4>
      <hr>
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
          </div>
          <input name="email" id="email" class="form-control" placeholder="Email" type="email" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
          </div>
          <input name="password" id="password" class="form-control" placeholder="******" type="password" required="required">
        </div>
      </div>
      <div class="form-group">
        <button name="login" id="login" class="btn btn-primary btn-block"> Ingresar </button>
      </div>
    </article>
  </div>
  <script src="../../js/jquery-3.4.1.min.js"></script>
  <script src="../../js/main.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>