-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-05-2019 a las 09:01:41
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `user_mvc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user_pk` int(11) NOT NULL,
  `name_user` varchar(100) NOT NULL,
  `phone_user` varchar(12) NOT NULL,
  `email_user` varchar(45) NOT NULL,
  `password_user` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user_pk`, `name_user`, `phone_user`, `email_user`, `password_user`) VALUES
(12, 'test 1', '8900000', 'test1@example.com', '$2y$12$GePd5KS3ZZPzNVDdAxsfRewwJZiPRYJXH1tMXC5PZUZKdsXQxdX.m'),
(13, 'test 2', '8900101', 'test2@example.com', '$2y$12$mg4aKcF6c9EMWTRXli8NseTFcnnSxF6Et8oH29URiNL1HiEdM0k96'),
(14, 'test 3', '8900202', 'test3@example.com', '$2y$12$ZW0dCxFO7SKHVWr4FKTwf..Rjqedwe/1tpdwaDvv6aJpIGtOpWmDy');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user_pk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user_pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
