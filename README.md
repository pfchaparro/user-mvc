# Usuarios MVC

Sistema de registro e ingreso de usuarios utilizando el lenguaje de programación PHP, empleando el patrón: modelo - vista - controlador (MVC).

### Pre-requisitos 📋

-APACHE
-MYSQL

## Comenzando 🚀

```
BASE DE DATOS
```
La base de datos `user_mvc.sql` debe cargarse en MySQL por medio del gestor PHPMyAdmin, la copia de la bd se encuentra en la carpeta `backup-bd`.

```
PROYECTO
```
El proyecto debe ir en la carpeta publica del servidor.
Por Ejemplo, si tiene xampp debe ir en `\xampp\htdocs\`; debe ejecutar los servicios de Apache y MySQL.

### Despliegue 🔧

Abrir en cualquier navegador el proyecto, ejemplo: `http:localhost/user-mvc`.

## Construido con 🛠️

* [PHP](https://php.net)
* [JAVASCRIPT](https://www.javascript.com/)
* [MySQL](https://www.mysql.com/)
## Autores ✒️

* **Pablo Felipe Chaparro Hurtado** - *Desarrollo* - *Documentación* - [pfchaparro](https://gitlab.com/pfchaparro)

---
⌨️ con ❤️ por [pfchaparro](https://gitlab.com/pfchaparro) 😊