<?php
$routeFolder = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$routeProject = explode("/", $routeFolder);

require_once $_SERVER['DOCUMENT_ROOT'] . "/" . $routeProject[1] . '/core/Connection.php';


class User extends Connection
{
  private $name;
  private $phone;
  private $email;
  private $password;

  public function __construct($name = null, $phone = null, $email = null, $password = null)
  {
    $this->name = $name;
    $this->phone = $phone;
    $this->email = $email;
    $this->password = $password;
    parent::__construct();
  }

  public function getAll()
  {
    try {
      $sql = $this->dbConnection->prepare("SELECT * FROM users");
      $sql->execute();
      $resultSet = null;

      while ($row = $sql->fetch(PDO::FETCH_OBJ)) {
        $resultSet[] = $row;
      }
      return $resultSet;
    } catch (PDOException $ex) {
      echo '<div class="alert alert-danger container text-center" role="alert">
                <strong>Error, Consulte al administrador del sistema.</strong>
            </div>';

      die();
    }
  }

  public function loginUser($email)
  {
    try {
      $sql = $this->dbConnection->prepare("SELECT * FROM users WHERE email_user=:email");
      $sql->execute(['email' => $email]);
      $resultSet = null;

      while ($row = $sql->fetch(PDO::FETCH_OBJ)) {
        $resultSet = $row;
      }
      
      return $resultSet;
      
    } catch (PDOException $ex) {
      echo '<div class="alert alert-danger container text-center" role="alert">
                <strong>Error, Consulte al administrador del sistema.</strong>
            </div>';

      die();
    }
  }

  public function insertUser()
  {
    try {
      $sql = $this->dbConnection->prepare("INSERT INTO users (name_user, phone_user, email_user, password_user)values(?,?,?,?)");
      $sql->bindParam(1, $this->name);
      $sql->bindParam(2, $this->phone);
      $sql->bindParam(3, $this->email);
      $sql->bindParam(4, $this->password);
      $sql->execute();

      return $sql;
    } catch (PDOException $ex) {
      echo '<div class="alert alert-danger container text-center" role="alert">
                <strong>Error, Consulte al administrador del sistema.</strong>
            </div>';

      die();
    }
  }

  public function deleteUser($id)
  {
    try {
      $dbUser = $this->dbConnection->prepare("DELETE FROM users where id_user_pk=" . $id);
      $dbUser->execute();
      return $dbUser;
    } catch (PDOException $ex) {
      echo '<div class="alert alert-danger container text-center" role="alert">
                <strong>Error, Consulte al administrador del sistema.</strong>
            </div>';

      die();
    }
  }

  public function showUser($id)
  {
    try {

      $sql = $this->dbConnection->prepare("SELECT * FROM users WHERE id_user_pk=" . $id);
      $sql->execute();
      $resultSet = null;
      while ($row = $sql->fetch(PDO::FETCH_OBJ)) {
        $resultSet = $row;
      }
      return $resultSet;
    } catch (PDOException $ex) {
      echo '<div class="alert alert-danger container text-center" role="alert">
                <strong>Error, Consulte al administrador del sistema.</strong>
            </div>';

      die();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  /**
   * Set the value of id
   *
   * @return  self
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Get the value of name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set the value of name
   *
   * @return  self
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get the value of phone
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * Set the value of phone
   *
   * @return  self
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;
    return $this;
  }

  /**
   * Get the value of email
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set the value of email
   *
   * @return  self
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * Get the value of password
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Set the value of password
   *
   * @return  self
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }
}
