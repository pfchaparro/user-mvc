/**
*
* validate the fields
*
**/
function validation() {
  var name = $("#name").val();
  var phone = $("#phone").val();
  var email = $("#email").val();
  var password = $("#password").val();

  if (name == "" || phone == "" || email == "" || password == "") {

    $("#name").attr("placeholder", "Ingrese un nombre");
    $("#name").addClass("val");

    $("#phone").attr("placeholder", "Ingrese un teléfono");
    $("#phone").addClass("val");

    $("#email").attr("placeholder", "Ingrese un correo");
    $("#email").addClass("val");

    $("#password").attr("placeholder", "Ingrese una contraseña");
    $("#password").addClass("val");

    return true;
  }
}

/**
*
* clean field
*
**/
function cleanField() {
  $("#name").val("");
  $("#phone").val("");
  $("#email").val("");
  $("#password").val("");
}
/**
*
* button login
*
**/
$("#login").click(function () {

  if (validation()) {

    validation();

  } else {
    var option = 'login';
    var email = $("#email").val();
    var password = $("#password").val();

    $.ajax({
      method: "POST",
      url: "../user/userProcess.php",
      data: {
        option: option,
        email: email,
        password: password
      },
      success: function (data) {
        var response = $.parseJSON(data);
        console.log(response);

        if (response.status == 'invalid') {
          $('#loginUser').modal('hide');
          $(".toast").toast("show");
          $("#toastMen").html(response.message);
        } else {
          location.href = '../user/listUsers.php';
        }
      },
      error: function (rest) {
        console.log(rest);
      }
    });
    cleanField();
  }
});

/**
*
* button create
*
**/
$("#sendData").click(function () {

  if (validation()) {

    validation();

  } else {
    var option = $("#option").val();
    var name = $("#name").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var password = $("#password").val();

    $.ajax({
      method: "POST",
      url: "userProcess.php",
      data: {
        option: option,
        name: name,
        phone: phone,
        email: email,
        password: password
      },
      success: function (response) {
        $('#createUser').modal('hide');
        $(".toast").toast("show");
        $("#toastMen").html(response);
        updatePage();
      },
      error: function (rest) {
        $('#createUser').modal('hide');
      }
    });
    cleanField();
  }
});

/**
*
* View User
*
**/
$(".viewUser").click(function () {
  var option = "view";
  var id = $(this).attr('id');

  $.ajax({
    method: "POST",
    url: "userProcess.php",
    data: {
      option: option,
      viewData: id
    },
    success: function (response) {
      $("#viewUser").html(response);
    },
    error: function (rest) {
      $("#viewUser").html(rest);

    }
  });

});

/**
*
* Close modal
*
**/
$(".viewClose").click(function () {
  $("#viewUser").empty();
});

/**
*
* Delete User.
*
**/
$(".deleteUser").click(function () {
  var id = $(this).attr('id');
  $('.del').html('¿Desea eliminar el usuario <strong style="color:blue" >' + id + '</strong> ?');
  $(".deleteSend").click(function () {

    $.ajax({
      method: "POST",
      url: "userProcess.php",
      data: {
        option: "delete",
        id: id

      },
      success: function (response) {
        $('#deleteUser').modal('hide');
        $(".toast").toast("show");
        $("#toastMen").html(response);
        updatePage();
      },
      error: function (rest) {
        $('#deleteUser').modal('hide');
      }
    });
  });
});

/**
*
* Close Session.
*
**/
$("#closeSession").click(function () {
  $.ajax({
    method: "POST",
    url: "userProcess.php",
    data: {
      option: "close"
    },
    success: function (response) {
      location.replace("../login/login.php");
    },
    error: function (rest) {
    }
  });
});

/**
*
* Refresh the page.
*
**/
function updatePage() {
  setTimeout(function () { window.location.reload(false); }, 6000);
}