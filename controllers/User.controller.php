<?php
$routeFolder = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$routeProject = explode("/", $routeFolder);

require_once $_SERVER['DOCUMENT_ROOT'] . "/" . $routeProject[1] . '/models/User.class.php';

class UserController
{

  public function loginUser($email)
  {
    $user_obj = new User($email);
    $user = $user_obj->loginUser($email);
    return $user;
  }

  public function insertUser($name, $phone, $email, $password)
  {
    $options = ['cost' => 12,];
    $passwordEncrypt = password_hash($password, PASSWORD_BCRYPT, $options);
    $user_obj = new User($name, $phone, $email, $passwordEncrypt);
    $user = $user_obj->insertUser();
    return $user;
  }

  public function deleteUser($id)
  {
    $user_obj = new user($id);
    $user = $user_obj->deleteUser($id);
    return $user;
  }

  public function showUser($id)
  {
    $user_obj = new user($id);
    $user = $user_obj->showUser($id);
    return $user;
  }

  public function listUsers()
  {
    $user_obj = new user();
    $users = $user_obj->getAll();
    return $users;
  }
}
